﻿Imports System.Data.OleDb
Public Class frmMain
    Dim dset As New DataSet
    Dim da As New OleDbDataAdapter
    Dim dcom As New OleDbCommand


    Private Sub join_table()
        da = New OleDbDataAdapter(" _ &
        SELECT tbl_student.fname,tbl_student.mname,tbl_student.lname, _ &
                tbl_student_info.barangay,tbl_student_info.municipality _ &
                    FROM tbl_student INNER JOIN tbl_student_info _ &
                        ON tbl_student.[ID]=tbl_student_info.stud_id;


        ", garapata)


        dset = New DataSet
        da.Fill(dset, 0)

        dgvJoin.DataSource = dset.Tables(0).DefaultView


    End Sub

    Private Sub get_data()
        da = New OleDbDataAdapter("SELECT * FROM tbl_student", garapata)
        dset = New DataSet
        da.Fill(dset, "tbl_student")

        dgvStudents.DataSource = dset.Tables("tbl_student").DefaultView


    End Sub
    Private Sub get_data_info(x As String)
        If (x <> "") Then
            If (lblChosen.Text <> "") Then
                da = New OleDbDataAdapter("SELECT * FROM tbl_student_info WHERE [stud_id]=" & Val(lblChosen.Text), garapata)

            End If
        Else
            da = New OleDbDataAdapter("SELECT * FROM tbl_student_info", garapata)



        End If


        dset = New DataSet
        da.Fill(dset, "tbl_student_info")

        dgvInfo.DataSource = dset.Tables("tbl_student_info").DefaultView
        Call stylemobulok()
    End Sub


    Private Sub stylemobulok()
        dgvInfo.Columns(0).Visible = False
        dgvInfo.Columns(1).Visible = False



    End Sub



    Private Sub FrmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call get_data()
        Call get_data_info("")

        Call join_table()

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If (MsgBox("Are you sureee?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes) Then
            Application.Exit()
        Else

        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub DgvStudents_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvStudents.CellClick
        If (e.RowIndex <> -1) Then
            lblChosen.Text = dgvStudents(0, e.RowIndex).Value.ToString()
        End If
        Call get_data_info(lblChosen.Text())


    End Sub
End Class
